#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7789.h> // Hardware-specific library for ST7789
#include <SPI.h>             // Arduino SPI library#include <Adafruit_GFX.h>    // Core graphics library

void testlines(uint16_t);
void testdrawtext(char *, uint16_t);
void testfastlines(uint16_t, uint16_t);
void testdrawrects(uint16_t);
void testfillrects(uint16_t, uint16_t);
void testfillcircles(uint8_t, uint16_t);
void testdrawcircles(uint8_t, uint16_t);
void testtriangles();
void testroundrects();
void tftPrintTest();
void mediabuttons();
